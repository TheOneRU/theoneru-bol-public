if player.charName ~= "Cassiopeia" or not VIP_USER then return end
require 'Prodiction'
local version = "YAC v0.80 tweaked by TheOneRU"

--[[
YetAnotherCass
by Vadash

0.80
 - new prediction
]]

----------------------------------------------------------------------------------------------------------------------
---------------- Minions, Heroes, Notification, Orbwalking, Spells, Buffs classes (Klokje)----------------------------
----------------------------------------------------------------------------------------------------------------------
NONE = 0
ALLY = 1
ENEMY = 2
NEUTRAL = 4
ALL = 7

class 'Colour' -- {

    function Colour.Get(table)
        return ARGB(table[1], table[2], table[3], table[4])
    end
	
-- }

class 'Minions' -- {
    
    Minions.tables = {
        [ALLY] = {},
        [ENEMY] = {},
        [NEUTRAL] = {},
    }

    Minions.instance = ""

    function Minions:__init()
        self.modeCount = 7
        self.tempMinions = {}
        self.mapIndex = GetGame().map.index

        for i = 1, objManager.maxObjects, 1 do
            local minion = objManager:GetObject(i)
            self:AddObject(minion)
        end
        AddCreateObjCallback(function(obj) self:OnCreateObj(obj) end)
    end

    function Minions:OnCreateObj(obj)
        self:AddObject(obj)
    end

    function Minions.Instance()
        if Minions.instance == "" then Minions.instance = Minions() end return Minions.instance 
    end

    function Minions:AddObject(obj)
        if obj and obj.valid and obj.type == "obj_AI_Minion" then

            DelayAction(function(obj)
                if obj.team == player.team then table.insert(Minions.tables[ALLY], obj) return end
                if obj.team == TEAM_ENEMY then table.insert(Minions.tables[ENEMY], obj) return end
                if obj.team == TEAM_NEUTRAL then table.insert(Minions.tables[NEUTRAL], obj) return end
            end, 0, {obj})
        end
    end 

    function Minions.GetObjects(mode, range, pFrom)
        return Minions.Instance():GetObjectsFromTable(mode, range, pFrom)
    end

    function Minions:GetObjectsFromTable(mode, range, pFrom)
        if mode > self.modeCount then mode = self.modeCount end 
        if range == nil or range < 0 then range = math.huge end
        if pFrom == nil then pFrom = player end
        tempTable = {}

        for i, tableType in pairs(Minions.tables) do
            if bit32.band(mode, i) == i then 
                for k,v in pairs(tableType) do 
                    if v and v.valid and not v.dead and v.health > 0 then 
                        if v.visible and (v.team == player.team or v.bInvulnerable == 0) and GetDistance(v, pFrom) <= range and v.bTargetable then table.insert(tempTable, v) end
                    else table.remove(tableType, k) k = k - 1 end
                end 
            end 
        end 
        return tempTable
    end
-- }

class 'Heroes' -- {
    
    Heroes.tables = {
        [ALLY] = {},
        [ENEMY] = {}
    }

    Heroes.instance = ""

    function Heroes:__init()
        self.modeCount = 3

        for i = 1, heroManager.iCount do
            local hero = heroManager:GetHero(i)
            self:AddObject(hero)
        end
    end

    function Heroes.Instance()
        if Heroes.instance == "" then Heroes.instance = Heroes() end return Heroes.instance 
    end

    function Heroes.GetObjects(mode, range, pFrom)
        return Heroes.Instance():GetObjectsFromTable(mode, range, pFrom)
    end

    function Heroes.GetAllObjects(mode)
        return Heroes.Instance():GetAllObjectsFromTable(mode)
    end

    function Heroes:AddObject(obj)
        if obj.team == player.team then
            table.insert(Heroes.tables[ALLY], obj)
        elseif obj.team == TEAM_ENEMY then
            table.insert(Heroes.tables[ENEMY], obj)
        end
    end 

    function Heroes.GetObjectByNetworkId(networkID)
         return Heroes.Instance():PrivateGetObjectByNetworkId(networkID)
    end

    function Heroes:PrivateGetObjectByNetworkId(networkID)
        for i, tableType in pairs(self.tables) do
            for k,v in pairs(tableType) do 
                if v.networkID == networkID then return v end 
            end 
        end
        return nil
    end 

    function Heroes:GetAllObjectsFromTable(mode)
        if mode > self.modeCount then mode = self.modeCount end 

        tempTable = {}

        for i, tableType in pairs(self.tables) do
            if bit32.band(mode, i) == i then 
                for k,v in pairs(tableType) do 
                    if v ~= nil then 
                        table.insert(tempTable, v)
                    end
                end 
            end 
        end 
        return tempTable
    end 

    function Heroes:GetObjectsFromTable(mode, range, pFrom)
        if mode > self.modeCount then mode = self.modeCount end 
        if range == nil or range < 0 then range = math.huge end
        if pFrom == nil then pFrom = player end
        tempTable = {}

        for i, tableType in pairs(self.tables) do
            if bit32.band(mode, i) == i then 
                for k,v in pairs(tableType) do 
                    if v ~= nil and v.valid and not v.dead then 
                        if v.visible and v.bTargetable and GetDistance(v, pFrom) <= range then table.insert(tempTable, v) end
                    end
                end 
            end 
        end 
        return tempTable
    end
-- }

local SortList = {
    ["Ashe"]= 1,["Caitlyn"] = 1,["Corki"] = 1,["Draven"] = 1,["Ezreal"] = 1,["Graves"] = 1,["Jayce"] = 1,["KogMaw"] = 1,["MissFortune"] = 1,["Quinn"] = 1,["Sivir"] = 1,
    ["Tristana"] = 1,["Twitch"] = 1,["Varus"] = 1,["Vayne"] = 1,["Lucian"] = 1,["Jinx"] = 1,

    ["Ahri"] = 2,["Annie"] = 2,["Akali"] = 2,["Anivia"] = 2,["Brand"] = 2,["Cassiopeia"] = 2,["Diana"] = 2,["Evelynn"] = 2,["FiddleSticks"] = 2,["Fizz"] = 2,["Gragas"] = 2,
    ["Heimerdinger"] = 2,["Karthus"] = 2,["Kassadin"] = 2,["Katarina"] = 2,["Kayle"] = 2,["Kennen"] = 2,["Leblanc"] = 2,["Lissandra"] = 2,["Lux"] = 2,["Malzahar"] = 2,["Zed"] = 2,
    ["Mordekaiser"] = 2,["Morgana"] = 2,["Nidalee"] = 2,["Orianna"] = 2,["Rumble"] = 2,["Ryze"] = 2,["Sion"] = 2,["Swain"] = 2,["Syndra"] = 2,["Teemo"] = 2,["TwistedFate"] = 2,
    ["Veigar"] = 2,["Viktor"] = 2,["Vladimir"] = 2,["Xerath"] = 2,["Ziggs"] = 2,["Zyra"] = 2,["MasterYi"] = 2,["Shaco"] = 2,["Jayce"] = 2,["Pantheon"] = 2,["Urgot"] = 2,["Talon"] = 2,
    
    ["Blitzcrank"] = 3,["Janna"] = 3,["Karma"] = 3,["Lulu"] = 3,["Nami"] = 3,["Nunu"] = 3,["Sona"] = 3,["Soraka"] = 3,["Thresh"] = 3,["Zilean"] = 3,

    ["Darius"] = 4,["Elise"] = 4,["Fiora"] = 4,["Gangplank"] = 4,["Irelia"] = 4,["JarvanIV"] = 4,["Jax"] = 4,["Khazix"] = 4,["LeeSin"] = 4,
    ["Olaf"] = 4,["Poppy"] = 4,["Renekton"] = 4,["Rengar"] = 4,["Riven"] = 4,["Tryndamere"] = 4,["Udyr"] = 4,["Vi"] = 4,["MonkeyKing"] = 4,
    ["Aatrox"] = 4,["Nocturne"] = 4,["XinZhao"] = 4,

    ["Amumu"] = 6,["Chogath"] = 6,["DrMundo"] = 6,["Galio"] = 6,["Hecarim"] = 6,["Malphite"] = 6,["Maokai"] = 6,["Nasus"] = 6,["Rammus"] = 6,["Sejuani"] = 6,["Shen"] = 6,
    ["Singed"] = 6,["Skarner"] = 6,["Volibear"] = 6,["Warwick"] = 6,["Yorick"] = 6,["Zac"] = 6, ["Taric"] = 6,
    ["Garen"] = 6, ["Alistar"] = 6,["Leona"] = 6, ["Nautilus"] = 6,["Shyvana"] = 6,["Trundle"] = 6,
}

local EnemySort = function(x,y)
    local dmgx = player:CalcMagicDamage(x, 100)
    local dmgy = player:CalcMagicDamage(y, 100)

    dmgx = dmgx/ (1 + (SortList[x.charName]/10) - 0.1) --dmgx = dmgx/ (1 + (SortList[x.charName]/10) - 0.1)
    dmgy = dmgy/ (1 + (SortList[y.charName]/10) - 0.1) --dmgy = dmgy/ (1 + (SortList[y.charName]/10) - 0.1)

    local valuex = x.health/dmgx
    local valuey = y.health/dmgy

    return valuex < valuey
end

local Sort = function(x,y) 
    return SortList[x.charName] < SortList[y.charName]
end

class 'Orbwalking' -- {

    Orbwalking.instance = ""

    function Orbwalking:__init()
        self.attacking = false
        self.nextAttack = 0
        self.windUp = 0
        self.windUpTime = 0
        self.disable = false
        self.target = nil
        self.spelldis = false

        self.lastPacket = nil
        
        AddTickCallback(function() self:OnTick() end)
        AddProcessSpellCallback(function(obj, spell) self:OnProcessSpell(obj, spell) end)
        AddSendPacketCallback(function(obj) self:OnSendPacket(obj) end)
    end

    function Orbwalking.Instance()
        if Orbwalking.instance == "" then Orbwalking.instance = Orbwalking() end return Orbwalking.instance 
    end

    function Orbwalking:OnTick()
        if self.attacking and self.windUp <= GetGameTimer() then 
            self.attacking = false

            if self.lastPacket ~= nil then
                SendPacket(tempPacket)
                self.lastPacket = nil
            end
        end
    end

    function Orbwalking:OnSendPacket(p)
        if self.attacking and not self.disable and not spelldis and (p.header == 0x71 or p.header == 0x9A) then
            self.lastPacket =  copyPacket(p)
            p.pos = 1
            p:Block()
        end
    end

    function Orbwalking:OnProcessSpell(object,spell)
        if object== nil or spell == nil then return end 

        if object.isMe and spell.name:find("Attack") then
            self.target = spell.target
            self.windUp = GetGameTimer() + spell.windUpTime
            self.windUpTime = spell.windUpTime
            self.nextAttack =  GetGameTimer() + spell.animationTime
            self.attacking = true
        end 
    end

    function Orbwalking.Enable(bool)
        Orbwalking.Instance().disable = not bool
    end

    function Orbwalking.CanAttack()
        return Orbwalking.Instance():PrivateCanAttack()
    end

    function Orbwalking.NextAttack()
        return Orbwalking.Instance().nextAttack
    end

    function Orbwalking.WindUp()
        return Orbwalking.Instance().windUp
    end

    function Orbwalking.WindUpTime()
        return Orbwalking.Instance().windUpTime
    end

    function Orbwalking.ResetAA()
        Orbwalking.Instance().nextAttack = 0
    end

    function Orbwalking.Attack(target)
        if _G.evade and _G.evade == true then return end
        return Orbwalking.Instance():PrivateAttack(target)
    end

    function Orbwalking:PrivateAttack(target)
        if self:PrivateCanAttack() then
            player:Attack(target)
            self.attacking = true
            return true 
        end
        return false
    end 

    function Orbwalking:PrivateCanAttack()
        return self.nextAttack <= GetGameTimer()
    end
-- }

function copyPacket(packet)
      packet.pos = 1
      p = CLoLPacket(packet.header)
      for i=1,packet.size-1,1 do
        p:Encode1(packet:Decode1())
      end
      p.dwArg1 = packet.dwArg1
      p.dwArg2 = packet.dwArg2
      return p
end
----------------------------------------------------------------------------------------------------------------------
-------------------------------------------- Klokje classes end ------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------- ON TICKs ------------------
function OnLoad()
    --[[ Optimize ]]--
    pairs = pairs
    ipairs = ipairs
    type = type
    sin = math.sin
    rad = math.rad
    cos = math.cos
    deg = math.deg
    atan = math.atan
    tableinsert = table.insert
    tableremove = table.remove
    tablesort = table.sort
    mathsqrt = math.sqrt

    --[[ Constants ]]--
    QDelay = 0.6--0.535
    WDelay = 0.375--0.375
    RDelay = 0.75
    QRangeS = 850
    QRangeL = 850 + 75
    WRangeS = 850
    WRangeL = 850 + 125
    ERange = 700
    RRange = 750 --850
	AARange = 700

    killable = {}
    enemyHeroes = {}
    ignite = nil
    qTick = 0
    qTar = nil
    ultCasting = false

    --[[ Ticks ]]--
    castRTick = 0
    harassTick = 0
    farmTick = 0
    passiveTick = 0

    --[[ Script Variables ]]--
    wp = ProdictManager.GetInstance()

    tpQ = wp:AddProdictionObject(_Q, QRangeL, 1800, 0.45, 75, myHero,
            function(target, vec1, castspell) 
                if GetDistance(vec1) <= QRangeS then
                    CastSpell(_Q, vec1.x, vec1.z)
                    qTick = GetTickCount()
                    qTar = target
                elseif GetDistance(vec1) <= QRangeL then
                    local vec2 = Vector(player) + (Vector(vec1) - Vector(player)):normalized() * QRangeS
                    if vec2 then
                        CastSpell(_Q, vec2.x, vec2.z)
                        qTick = GetTickCount()
                        qTar = target
                    end
                end
            end)
    tpW = wp:AddProdictionObject(_W, WRangeL, 1800, 0.5, 75, myHero, 
            function(target, vec1, castspell)
                if GetDistance(vec1) <= WRangeS then
                    CastSpell(_W, vec1.x, vec1.z)
                elseif GetDistance(vec1) <= WRangeL then
                    local vec2 = Vector(player) + (Vector(vec1) - Vector(player)):normalized() * WRangeS
                    if vec2 then
                        CastSpell(_W, vec2.x, vec2.z)
                    end
                end
            end)
    tpR = TargetPredictionVIP(RRange, 2000, RDelay)
    tpR1 = wp:AddProdictionObject(_R, RRange, 2000, RDelay, 0, myHero, 
            function(target, vec, castspell)
                enemyPos = tpR:GetPrediction(target)
				if enemyPos ~= nil then
					if GetDistance(enemyPos) < GetDistance(target) and IsValid(target, RRange) and GetDistance(vec) <= RRange * 3/4 and GetDistance(vec) > RRange * 1/4 then
						CastSpell(_R, vec.x, vec.z)
					end
				end
            end)
    
    for i = 1, heroManager.iCount do
        local hero = heroManager:GetHero(i)
        if hero.team ~= player.team then
            tableinsert(enemyHeroes, hero)
        end
    end

    Minions.Instance()
    Orbwalking.Instance()
	loadMenus()

    if player:GetSpellData(SUMMONER_1).name:find("SummonerDot") then ignite = SUMMONER_1
    elseif player:GetSpellData(SUMMONER_2).name:find("SummonerDot") then ignite = SUMMONER_2
    end

    PrintChat("<font color='#CCCCCC'> >> YAC loaded! <<</font>")
    PrintChat(version)
end

function loadMenus()
	settingsMenu()
	drawingMenu()
	colourMenu()
	displayMenu()
	permaShow()
end

function settingsMenu()
	YACmain = scriptConfig("YAC: Main Settings", "YAC_settings")
	YACmain:addParam("info", "               Yet Another Cassiopeia", SCRIPT_PARAM_INFO, "")
    YACmain:addParam("combo", "COMBO", SCRIPT_PARAM_ONKEYDOWN, false, 32)
    YACmain:addParam("castUlt", "Cast Ult", SCRIPT_PARAM_ONKEYDOWN, false, string.byte("R"))
    YACmain:addParam("Harass1", "Q+E Harass", SCRIPT_PARAM_ONKEYDOWN, false, string.byte("S"))
    YACmain:addParam("Harass2", "Q Harass (Escape Mode)", SCRIPT_PARAM_ONKEYDOWN, false, string.byte("A"))
	YACmain:addParam("ThreeKEloUlt", "Use Ult on Single Target", SCRIPT_PARAM_ONKEYTOGGLE, false, string.byte("D"))
	YACmain:addParam("autoFarm", "Auto Farm with E", SCRIPT_PARAM_ONKEYTOGGLE, false, string.byte("T"))
	YACmain:addParam("autoPT", "Passive Tracking", SCRIPT_PARAM_ONKEYTOGGLE, false, string.byte("U"))
    YACmain:addParam("autoE", "Auto E on Poisoned Enemies", SCRIPT_PARAM_ONKEYTOGGLE, false, string.byte("Y"))
    YACmain:addParam("setUltEnemiesSBTW", "Ult Weight (in SBTW Combo)", SCRIPT_PARAM_SLICE, 2, 1, 6, 0)
    YACmain:addParam("setUltEnemiesAUTO", "Ult Weight (Automatic)", SCRIPT_PARAM_SLICE, 3, 1, 6, 0)
    YACmain:addParam("moveToMouse", "Move To Mouse", SCRIPT_PARAM_ONOFF, true)
end

function drawingMenu()
	YACdraw = scriptConfig("YAC: Draw Settings", "YAC_drawing")
	YACdraw:addParam("DisableDrawing", "Disable All Drawing", SCRIPT_PARAM_ONOFF, false)
	YACdraw:addParam("permaCircle", "Always Draw Range Circles", SCRIPT_PARAM_ONOFF, false)
    YACdraw:addParam("drawQrange", "Draw Q Range Circle", SCRIPT_PARAM_ONOFF, true)
	YACdraw:addParam("drawWrange", "Draw W Range Circle", SCRIPT_PARAM_ONOFF, true)
	YACdraw:addParam("drawErange", "Draw E Range Circle", SCRIPT_PARAM_ONOFF, true)
	YACdraw:addParam("drawRrange", "Draw R Range Circle", SCRIPT_PARAM_ONOFF, true)
	YACdraw:addParam("drawAArange", "Draw AA Range Circle", SCRIPT_PARAM_ONOFF, false)
	YACdraw:addParam("drawtargetcircle", "Draw Circle Around Selected Target", SCRIPT_PARAM_ONOFF, true)
	YACdraw:addParam("drawtarget", "Draw Text on Selected Target", SCRIPT_PARAM_ONOFF, true)
	YACdraw:addParam("drawtargettext", "Draw Target Notification Text", SCRIPT_PARAM_ONOFF, true)
end

function colourMenu()
	YACcol = scriptConfig("YAC: Colour Settings", "YAC_colour")
	YACcol:addParam("QColour", "Q Circle Colour", SCRIPT_PARAM_COLOR, {255, 51, 204, 255})
	YACcol:addParam("WColour", "W Circle Colour", SCRIPT_PARAM_COLOR, {255, 204, 51, 255})
	YACcol:addParam("EColour", "E Circle Colour", SCRIPT_PARAM_COLOR, {255, 6, 157, 8})
	YACcol:addParam("RColour", "R Circle Colour", SCRIPT_PARAM_COLOR, {255, 191, 3, 50})
	YACcol:addParam("AAColour", "AA Circle Colour", SCRIPT_PARAM_COLOR, {255, 255, 255, 255})
	YACcol:addParam("targetCircle", "Target Circle Colour", SCRIPT_PARAM_COLOR, {255, 246, 105, 4})
	YACcol:addParam("targetText", "Target Text Colour", SCRIPT_PARAM_COLOR, {255, 246, 105, 4})	
end

function displayMenu()
	YACdisp = scriptConfig("YAC: Display Settings", "YAC_display")
	YACdisp:addParam("info", "---- Changes Require Reload ----", SCRIPT_PARAM_INFO, "")
	YACdisp:addParam("disableAll", ">> DISABLE ALL <<", SCRIPT_PARAM_ONOFF, false)
	YACdisp:addParam("combo", "Display Combo Status", SCRIPT_PARAM_ONOFF, true)
	YACdisp:addParam("Harass1", "Display Q+E Harass Status", SCRIPT_PARAM_ONOFF, true)
	YACdisp:addParam("Harass2", "Display Q Harass Status", SCRIPT_PARAM_ONOFF, true)
	YACdisp:addParam("ThreeKEloUlt", "Display Single Target Ult Status", SCRIPT_PARAM_ONOFF, true)
	YACdisp:addParam("autoFarm", "Display Auto Farm Status", SCRIPT_PARAM_ONOFF, true)
	YACdisp:addParam("autoE", "Display Auto E Status", SCRIPT_PARAM_ONOFF, true)
	YACdisp:addParam("autoPT", "Display Passive Tracking Status", SCRIPT_PARAM_ONOFF, true)
end

function permaShow()
	if not YACdisp.disableAll then
		YACmain:permaShow("info")
		if YACdisp.combo then YACmain:permaShow("combo") end
		if YACdisp.Harass1 then YACmain:permaShow("Harass1") end
		if YACdisp.Harass2 then YACmain:permaShow("Harass2") end
		if YACdisp.ThreeKEloUlt then YACmain:permaShow("ThreeKEloUlt") end
		if YACdisp.autoFarm then YACmain:permaShow("autoFarm") end
		if YACdisp.autoE then YACmain:permaShow("autoE") end
		if YACdisp.autoPT then YACmain:permaShow("autoPT") end
	end
end

function OnRecall(hero, channelTimeInMs)    -- gets triggered when somebody starts to recall
    if hero.networkID == player.networkID then
        IsRecaling = true
    end
end
function OnAbortRecall(hero)                -- gets triggered when somebody aborts a recall
    if hero.networkID == player.networkID then
        IsRecaling = false
    end 
end
function OnFinishRecall(hero)               -- gets triggered when somebody finishes a recall
    if hero.networkID == player.networkID then
        IsRecaling = false
    end
end

function OnTick()
    if not YACmain.combo then Orbwalking.Enable(false) else Orbwalking.Enable(true) end
    if _G.evade and _G.evade == true then return end

    if qTar and not IsValid(qTar, QRangeL) then qTar = nil end
    if GetTickCount() - qTick > 3000 then qTar = nil end
    
    enemyHeroes = Heroes.GetObjects(ENEMY, 3000)
    tablesort(enemyHeroes, EnemySort)

    AutoUlt()

    if YACmain.castUlt then CastR1(nil) end

    CalculateDmg()
    if YACmain.combo then BurstCombo() SustainedCombo() elseif YACmain.autoE then AutoE() end

    if YACmain.Harass1 and not YACmain.combo and not YACmain.Harass2 then Harass1() end
        
    if YACmain.Harass2 and not YACmain.combo and not YACmain.Harass1 then Harass2() end

    if not YACmain.combo and YACmain.autoFarm then FarmMinions() end

    if not YACmain.combo and YACmain.autoPT then PassiveTracking() end

    if YACmain.combo and YACmain.moveToMouse then MoveToCursor() end
        
    if YACmain.Harass2 and YACmain.moveToMouse and not YACmain.Harass1 then MoveToCursor() end
end

function OnDrawRanges()
	if YACdraw.drawQrange and player:CanUseSpell(_Q) == READY then
		DrawCircle(player.x, player.y, player.z, QRangeL, Colour.Get(YACcol.QColour))
	end
	if YACdraw.drawWrange and player:CanUseSpell(_W) == READY then
		DrawCircle(player.x, player.y, player.z, WRangeL, Colour.Get(YACcol.WColour))
	end
	if YACdraw.drawErange and player:CanUseSpell(_E) == READY then
		DrawCircle(player.x, player.y, player.z, ERange, Colour.Get(YACcol.EColour))
	end
	if YACdraw.drawRrange and player:CanUseSpell(_R) == READY then
		DrawCircle(player.x, player.y, player.z, RRange, Colour.Get(YACcol.RColour))
	end
	if YACdraw.drawAArange then
		DrawCircle(player.x, player.y, player.z, AARange, Colour.Get(YACcol.AAColour))
	end
end

function PermaDrawRanges()
	if YACdraw.drawQrange then
		DrawCircle(player.x, player.y, player.z, QRangeL, Colour.Get(YACcol.QColour))
	end
	if YACdraw.drawWrange then
		DrawCircle(player.x, player.y, player.z, WRangeL, Colour.Get(YACcol.WColour))
	end
	if YACdraw.drawErange then
		DrawCircle(player.x, player.y, player.z, ERange, Colour.Get(YACcol.EColour))
	end
	if YACdraw.drawRrange then
		DrawCircle(player.x, player.y, player.z, RRange, Colour.Get(YACcol.RColour))
	end
	if YACdraw.drawAArange then
		DrawCircle(player.x, player.y, player.z, AARange, Colour.Get(YACcol.AAColour))
	end
end

function OnDrawTarget()
	if YACdraw.drawtargetcircle then
		for i=1, #enemyHeroes do
			local target = enemyHeroes[i]
			if IsValid(target, 1200) then
				DrawCircle(target.x, target.y, target.z, 100, Colour.Get(YACcol.targetCircle))
				break
			end
		end
    end
	if YACdraw.drawtargettext then
		for i=1, #enemyHeroes do
			local target = enemyHeroes[i]
			if IsValid(target, 1200) then
				DrawText3D("Targetting: " .. target.charName, player.x, player.y, player.z, 16, Colour.Get(YACcol.targetText), true)
				break
			end
		end
    end
	if YACdraw.drawtarget then
		for i=1, #enemyHeroes do
			local target = enemyHeroes[i]
			if IsValid(target, 1200) then
				DrawText3D("TARGET", target.x, target.y, target.z, 16, Colour.Get(YACcol.targetText), true)
				break
			end
		end
    end
end

function OnDraw()
	if not YACdraw.DisableDrawing and not YACdraw.permaCircle then
		OnDrawRanges()
		OnDrawTarget()
    end
	if not YACdraw.DisableDrawing and YACdraw.permaCircle then
		PermaDrawRanges()
		OnDrawTarget()
	end		
end

local lastAnimation = ""
local lastSpellTime = 0
function OnAnimation(unit, animationName)
    if unit.isMe and lastAnimation ~= animationName then 
        lastAnimation = animationName
        if lastAnimation == "Spell1" or lastAnimation == "Spell2" or lastAnimation == "Spell3" or lastAnimation == "Spell4" then
            lastSpellTime = GetGameTimer()
            IsRecaling = false
        end 
    end
end

------------------------------------------------------------------- MAIN FUNCs -----------------
function AutoUlt()
    if GetTickCount()-castRTick >= 101 then -- auto ults
        castRTick = GetTickCount()
        if YACmain.combo then
            if YACmain.setUltEnemiesSBTW > 0 then
                CastR(YACmain.setUltEnemiesSBTW)
            end
        else
            if YACmain.setUltEnemiesAUTO > 0 then
                CastR(YACmain.setUltEnemiesAUTO)
            end
        end
    end
end

local STEP = 0
local lockedTarget = nil
function DoStep(value, desiredSpell, needIgnite, last)
    if needIgnite then CastIgnite(lockedTarget) end
    if _checkStep(value, desiredSpell) then
        if not last then
            _nextStep(desiredSpell)         
        else
            _lastStep(desiredSpell)         
        end
    end
end

function _checkStep(value, desiredSpell)
    if desiredSpell == _E then
        return player:CanUseSpell(_E) == READY and STEP == value
    elseif desiredSpell == _Q or desiredSpell == _W then 
        return player:CanUseSpell(desiredSpell) == READY and STEP == value
    else
        print("wrong spell")
    end
end

function _nextStep(desiredSpell)
    if desiredSpell == _Q then
        CastQ(lockedTarget)
        if player:CanUseSpell(desiredSpell) ~= READY then STEP = STEP + 1 end
    elseif desiredSpell == _W then
        CastW(lockedTarget)
        if player:CanUseSpell(desiredSpell) ~= READY then STEP = STEP + 1 end
    elseif desiredSpell == _E then
        if IsPoisoned(lockedTarget) then CastE(lockedTarget) end
        if player:CanUseSpell(desiredSpell) ~= READY then STEP = STEP + 1 end
    end
end

function _lastStep(desiredSpell)
    if desiredSpell == _Q then
        CastQ(lockedTarget)
        if player:CanUseSpell(desiredSpell) ~= READY then STEP = 0 lockedTarget = nil end
    elseif desiredSpell == _W then
        CastW(lockedTarget)
        if player:CanUseSpell(desiredSpell) ~= READY then STEP = 0 lockedTarget = nil end
    elseif desiredSpell == _E then
        if IsPoisoned(lockedTarget) then CastE(lockedTarget) else STEP = 0 lockedTarget = nil end
        if player:CanUseSpell(desiredSpell) ~= READY then STEP = 0 lockedTarget = nil end
    end
end

function BurstCombo() -- 1 E, 2 Q EEE, 3 Q W EEE Ignite, 4 - same as "3" + R, 5 same as "4" but need mana and cd
    if lockedTarget and (GetDistance(lockedTarget) > 950 or not IsValid(lockedTarget, 950)) then
        STEP = 0
        lockedTarget = nil
        return
    end
    Orbwalking.Enable(false)

    DoStep(100, _W, false, false)
    DoStep(101, _Q, false, false)
    DoStep(102, _E, false, false)
    DoStep(103, _E, false, false)
    DoStep(104, _E, false, true)

    if STEP >= 200 and STEP < 210 and player:CanUseSpell(_R) == READY then
        CastR1(lockedTarget)
    end
    if STEP >= 200 and STEP < 210 and player:CanUseSpell(_W) == READY then
        CastW(lockedTarget)
    end 
    DoStep(200, _Q, false, false)
    DoStep(201, _E, false, false)
    DoStep(202, _E, false, false)
    DoStep(203, _E, false, true)

    DoStep(300, _Q, true, false)
    DoStep(301, _E, true, false)
    DoStep(302, _E, true, false)
    DoStep(303, _E, true, true)

    if STEP >= 500 and STEP < 510 and player:CanUseSpell(_R) == READY then
        CastR1(lockedTarget)
    end
    if STEP >= 500 and STEP < 510 and player:CanUseSpell(_W) == READY then
        CastW(lockedTarget)
    end

    DoStep(500, _Q, true, false)
    DoStep(501, _E, true, false)
    DoStep(502, _E, true, false)
    DoStep(503, _E, true, true)

    Orbwalking.Enable(true)
    local qReady = player:CanUseSpell(_Q) == READY
    if lockedTarget and IsValid(lockedTarget, 550) and not qReady and lockedTarget.armor < 80 then
        if GetDistance(lockedTarget) < 550 then
            Orbwalking.Attack(lockedTarget)
        end
    end
end

function _setStep(target, value)
    if IsValid(target, 1200) and STEP == 0 and not lockedTarget then STEP = value lockedTarget = target end
end

function CalculateDmg()
    for i=1, #enemyHeroes do
        local target = enemyHeroes[i]   
        if IsValid(target, 3000) then
            --Buffs:Test1(target)
            local aDmg = getDmg("AD",target, player)
            local qDmg = getDmg("Q", target, player)
            local qReady = player:CanUseSpell(_Q) == READY
            local qMana = player:GetSpellData(_Q).mana
            local wDmg = getDmg("W", target, player)
            local wReady = player:CanUseSpell(_W) == READY
            local wMana = player:GetSpellData(_W).mana
            local eDmg = getDmg("E", target, player)
            local eReady = player:CanUseSpell(_E) == READY
            local eMana = player:GetSpellData(_E).mana
            local rDmg = getDmg("R", target, player)
            local rReady = player:CanUseSpell(_R) == READY
            local rMana = player:GetSpellData(_R).mana
            local ignitedmg = 0
            if ignite ~= nil and player:CanUseSpell(ignite) == READY then ignitedmg = getDmg("IGNITE", target, player) end
            local health = target.health + ((target.hpRegen/5) * 1)
            local mana = player.mana

            if eReady and CountEnemyHeroInRange(1000) == 1 and health < eDmg and mana > eMana then -- KS
                killable[target.networkID] = 1
                Orbwalking.Enable(false)
                CastE(target)
                Orbwalking.Enable(true)
            end
            
            if qReady and wReady and eReady and health < qDmg + wDmg + 4 * eDmg then
                -- W Q EEEE
                killable[target.networkID] = 1
                _setStep(target, 100)                               
            elseif (rReady and qReady and eReady and health < rDmg + qDmg + 4 * eDmg) or (rReady and qReady and wReady and eReady and health < rDmg + qDmg + wDmg + 4 * eDmg) then
                -- R (W)Q EEEE
                killable[target.networkID] = 2
                if YACmain.ThreeKEloUlt then _setStep(target, 200) end                                  
            elseif ignitedmg > 0 and qReady and eReady and health > qDmg + 2 * eDmg and health < qDmg + 4 * eDmg + ignitedmg then
                -- Q(W) EEEE Ign
                killable[target.networkID] = 2
                _setStep(target, 300)
            elseif (ignitedmg > 0 and rReady and qReady and eReady and health < rDmg + qDmg + 4 * eDmg + ignitedmg) 
                or (ignitedmg > 0 and rReady and qReady and wReady and eReady and health < rDmg + qDmg + wDmg + 4 * eDmg + ignitedmg) then
                killable[target.networkID] = 3
                -- R (W)Q EEEE + Ign
                if YACmain.ThreeKEloUlt then _setStep(target, 500) end                          
            else
                killable[target.networkID] = 0
            end
        end
    end
end

function SustainedCombo() -- Q/W non poisoned targets + E poisoned targets
    Orbwalking.Enable(false)
    local qReady = player:CanUseSpell(_Q) == READY
    local wReady = player:CanUseSpell(_W) == READY
    local eReady = player:CanUseSpell(_E) == READY
    for i=1, #enemyHeroes do
        local target = enemyHeroes[i]
        if IsValid(target, 1000) then
            if eReady and IsPoisoned(target) and CastE(target) then
                break
            end
        end
    end
    for i=1, #enemyHeroes do
        local target = enemyHeroes[i]
        if IsValid(target, 1200) then
            if qReady and CastQ(target) then
                break
            end
            if wReady and not IsPoisoned(target) and CastWSafe(target) then
                break
            end
        end
    end
    Orbwalking.Enable(true)
end

function AutoE() -- auto E poisoned enemies
    --Orbwalking.Enable(false)
    for i=1, #enemyHeroes do
        local target = enemyHeroes[i]
        if IsValid(target, ERange) and IsPoisoned(target) then
            CastE(target)
            break
        end
    end
    --Orbwalking.Enable(true)
end

function Harass1()
    --if STEP ~= 0 then return end
    if (harassTick == nil or GetTickCount()-harassTick >= 51) then
        harassTick = GetTickCount()
        for i=1, #enemyHeroes do
            local target = enemyHeroes[i]
            if player:CanUseSpell(_Q) == READY and IsValid(target, 1200) then
                tpQ:EnableTarget(target, true)
            end
                        if IsValid(target, ERange) and IsPoisoned(target) then
                                CastE(target)
                                break
                        end
        end
    end
end

function Harass2()
    --if STEP ~= 0 then return end
    if (harassTick == nil or GetTickCount()-harassTick >= 51) then
        harassTick = GetTickCount()
        for i=1, #enemyHeroes do
            local target = enemyHeroes[i]
            if player:CanUseSpell(_Q) == READY and IsValid(target, 1200) then
                tpQ:EnableTarget(target, true)
            end
        end
    end
end

function PassiveTracking()
    if passiveTick == nil or GetTickCount()-passiveTick >= 91 then
        passiveTick = GetTickCount()
        if not IsRecaling then
            local time = GetGameTimer() - lastSpellTime
            if time > 3 then
                local enemies = Heroes.GetObjects(ENEMY, QRangeL) 
                if player:CanUseSpell(_Q) == READY and #enemies > 0 then
                    for i, target in pairs(enemies) do
                        if CastQ(target) then
                            time = -1   
                        end
                    end 
                end
            end
            if time > 4 then
                local allyMinions = Minions.GetObjects(ALLY, 1500)
                local enemyMinions = Minions.GetObjects(ENEMY, 1500)
                if player:CanUseSpell(_Q) == READY and (#enemyMinions > #allyMinions or CountEnemyHeroInRange(1000) == 0) then
                    for i, target in pairs(enemyMinions) do
                        if GetDistance(target) < QRangeS then
                            CastSpell(_Q, target.x, target.z)
                            time = -1
                            break
                        end
                    end
                end
            end
            if time > 4.8 then
                if player:CanUseSpell(_Q) == READY then
                    local checksPos = player - (Vector(player) - mousePos):normalized()*(400)
                    CastSpell(_Q, checksPos.x, checksPos.z)
                    time = -1
                end
            end     
        end
    end
end

function OnTowerFocus(tower, target)
    if not YACmain.combo and YACmain.autoFarm and GetDistance(target) < ERange then
        local aDmg = getDmg("AD",target, player)
        local eDmg = getDmg("E", target, player)
        local eReady = player:CanUseSpell(_E) == READY
        local poisoned = IsPoisoned(target)

        if Orbwalking.CanAttack() and GetDistance(target) < 550 and target.health < (aDmg + 4) * 1.05 then
            Orbwalking.Attack(target)
        elseif poisoned and eReady and target.health <= eDmg * 1.05 then
            CastE(target)
        elseif Orbwalking.CanAttack() and GetDistance(target) < 550 and poisoned and eReady and target.health <= (eDmg + aDmg + 4) * 1.05 then
            Orbwalking.Attack(target)
            CastE(target)           
        end
    end
end

function FarmMinions()
    if farmTick == nil or GetTickCount()-farmTick >= 47 then
        farmTick = GetTickCount()
        local eReady = player:CanUseSpell(_E) == READY
        if eReady then
            local minions = Minions.GetObjects(ENEMY, ERange)
            for i, target in pairs(minions) do
                local aDmg = getDmg("AD",target, player)
                local eDmg = getDmg("E", target, player)
                local poisoned = IsPoisoned(target)
                if poisoned and target.maxHealth >= 700 and target.health <= eDmg then --cannon minion
                    CastE(target)
                    break
                end                
                if #minions == 1 and poisoned and target.health <= eDmg then --low wave
                    CastE(target)
                    break
                end
                if not Orbwalking.CanAttack() and poisoned and target.health <= (aDmg + 4) * 1.05 * 1.30 -- 30% safe range
                    and (not AutoCarry.GetKillableMinion() or AutoCarry.GetKillableMinion().networkID ~= target.networkID) then 
                    CastE(target)
                    break
                end
            end
        end
    end
end

function MoveToCursor()
    local x = mousePos.x
    local y = mousePos.z
    local selectionRadius = 65
    local destination = Point(x, y)
    local heroPos = Point(myHero.x, myHero.y)
    if heroPos:distance(destination) <= selectionRadius then
        local moveTo = heroPos + (destination - heroPos):normalized() * ( selectionRadius + 30 )
        player:MoveTo(moveTo.x, moveTo.y)
    else
        player:MoveTo(x, y)
    end
end

function IsPoisoned(target)
    local delay = math.max(GetDistance(target), 700)/1800 + 0.125
    for i = 1, target.buffCount do
        local tBuff = target:getBuff(i)
        if BuffIsValid(tBuff) and (tBuff.name == "cassiopeianoxiousblastpoison" or tBuff.name == "cassiopeiamiasmapoison" 
            or tBuff.name == "toxicshotparticle" or tBuff.name == "bantamtraptarget" or tBuff.name == "poisontrailtarget" 
            or tBuff.name == "deadlyvenom") and tBuff.endT - delay - GetGameTimer() > 0 then
            return true
        end
    end 
    return false
end

function CastQ(target)
    if player:CanUseSpell(_Q) == READY and IsValid(target, 1200) then
        tpQ:EnableTarget(target, true)
        return true
    end
    return false
end

function CastW(target)
    if player:CanUseSpell(_W) == READY and IsValid(target, 1200) then
        tpW:EnableTarget(target, true)
        return true
    end
    return false
end

function CastWSafe(target)
    if (player:CanUseSpell(_W) == READY and player:CanUseSpell(_Q) ~= READY and GetTickCount()-qTick >= 750 
        and not TargetHaveBuff("cassiopeianoxiousblasthaste", player)) 
        or (player:CanUseSpell(_W) == READY and player:CanUseSpell(_Q) ~= READY and (qTar == nil or qTar ~= target)) then    
            tpW:EnableTarget(target, true)
            return true
    end
    return false
end

function CastE(target)
    if player:CanUseSpell(_E) == READY and IsValid(target, ERange) then
        CastSpell(_E, target)
        return true
    end
    return false
end

function CastR1(target)
    if target == nil then
        for i=1, #enemyHeroes do
            if IsValid(enemyHeroes[i], RRange) then
                target = enemyHeroes[i]
                break
            end
        end
    end
    if target == nil then return end
    if player:CanUseSpell(_R) == READY then
        tpR1:EnableTarget(target, true)
    end
end

function CastR(n) -- Cast Ult to n
    Orbwalking.Enable(false)
    if player:CanUseSpell(_R) == READY then
        local ultEnemies = CountEnemyHeroInRange(RRange + 300)
        if ultEnemies >= n then
            local vec = GetCassMECS(75, RRange, n, false) -- 80 degree R
            if vec ~= nil and GetDistance(vec) < RRange then
                CastSpell(_R, vec.x, vec.z)
            end
        end
    end
    Orbwalking.Enable(true)
end


function CastIgnite(target)
    if ignite ~= nil and IsValid(target, 600) and player:CanUseSpell(ignite) == READY and GetDistance(target) < 600 then
        CastSpell(ignite, target)
        return true
    end
    return false
end

function IsValid(target, dist)
    if target ~= nil and target.valid and not target.dead and target.bTargetable and ValidTarget(target, dist) then
        return true
    else
        return false
    end
end

----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------- iLib START------------------------------------------
----------------------------------------------------------------------------------------------------------------------
function areClockwise(testv1,testv2)
    return -testv1.x * testv2.y + testv1.y * testv2.x>0 --true if v1 is clockwise to v2
end
function sign(x)
    if x> 0 then return 1
    elseif x<0 then return -1
    end
end

local mecCheckTick = 0
function GetCassMECS(theta, radius, minimum, bForce)
    if GetTickCount() - mecCheckTick < 100 then return nil end
    mecCheckTick = GetTickCount()   
    --Build table of enemies in range
    nFaced = 0
    n = 1
    v1,v2,v3 = 0,0,0
    largeN,largeV1,largeV2 = 0,0,0
    theta1,theta2,smallBisect = 0,0,0
    coneTargetsTable = {}
   
    for i = 1, heroManager.iCount, 1 do
        hero = heroManager:getHero(i)
        enemyPos = tpR:GetPrediction(hero)
        if ValidTarget(hero, 1000) and enemyPos and GetDistance(enemyPos) < radius then-- and inRadius(hero,radius*radius) then
            coneTargetsTable[n] = hero
            n=n+1
			if hero.visionPos ~= nil then
				if (GetDistance(hero.visionPos) < GetDistance(hero)) 
					or (killable[hero.networkID] >= 1 and killable[hero.networkID] <= 4) 
					or bForce == true then
					nFaced = nFaced + 1
				else
					nFaced = nFaced + 0.67
				end
			end            
        end
    end

    if #coneTargetsTable>=2 then -- true if calculation is needed
    --Determine if angle between vectors are < given theta
            for i=1, #coneTargetsTable,1 do
                    for j=1,#coneTargetsTable, 1 do
                            if i~=j then
                                    --Position vector from player to 2 different targets.
                                    v1 = Vector(coneTargetsTable[i].x-player.x , coneTargetsTable[i].z-player.z)
                                    v2 = Vector(coneTargetsTable[j].x-player.x , coneTargetsTable[j].z-player.z)
                                    thetav1 = sign(v1.y)*90-math.deg(math.atan(v1.x/v1.y))
                                    thetav2 = sign(v2.y)*90-math.deg(math.atan(v2.x/v2.y))
                                    thetaBetween = thetav2-thetav1                 

                                    if (thetaBetween) <= theta and thetaBetween>0 then --true if targets are close enough together.
                                            if #coneTargetsTable == 2 then --only 2 targets, the result is found.
                                                    largeV1 = v1
                                                    largeV2 = v2
                                            else
                                                    --Determine # of vectors between v1 and v2                                                     
                                                    tempN = 0
                                                    for k=1, #coneTargetsTable,1 do
                                                            if k~=i and k~=j then
                                                                    --Build position vector of third target
                                                                    v3 = Vector(coneTargetsTable[k].x-player.x , coneTargetsTable[k].z-player.z)
                                                                    --For v3 to be between v1 and v2
                                                                    --it must be clockwise to v1
                                                                    --and counter-clockwise to v2
                                                                    if areClockwise(v3,v1) and not areClockwise(v3,v2) then
                                                                            tempN = tempN+1
                                                                    end
                                                            end
                                                    end
                                                    if tempN > largeN then
                                                    --store the largest number of contained enemies
                                                    --and the bounding position vectors
                                                            largeN = tempN
                                                            largeV1 = v1
                                                            largeV2 = v2
                                                    end
                                            end
                                    end
                            end
                    end
            end
    elseif #coneTargetsTable==1 and minimum == 1 then
            return coneTargetsTable[1]
    end
   
    if largeV1 == 0 or largeV2 == 0 then
    --No targets or one target was found.
            return nil
    else
            --small-Bisect the two vectors that encompass the most vectors.
            if largeV1.y == 0 then
                    theta1 = 0
            else
                    theta1 = sign(largeV1.y)*90-math.deg(math.atan(largeV1.x/largeV1.y))
            end
            if largeV2.y == 0 then
                    theta2 = 0
            else
                    theta2 = sign(largeV2.y)*90-math.deg(math.atan(largeV2.x/largeV2.y))
            end

            smallBisect = math.rad((theta1 + theta2) / 2)
            vResult = {}
            vResult.x = radius*math.cos(smallBisect)+player.x
            vResult.y = player.y
            vResult.z = radius*math.sin(smallBisect)+player.z
            
            return vResult
    end
end
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------- iLib END--------------------------------------------
----------------------------------------------------------------------------------------------------------------------

function OnWndMsg(msg, key) 
    if key == 32 then
        if msg == KEY_DOWN then
                    qTick = 0
                    qTar = nil
                    ultCasting = false
                STEP = 0
                lockedTarget = nil
        end
    end
end