if FileExist(LIB_PATH.."FHPrediction.lua") then
	require("FHPrediction") 
else
	PrintChat("<font color='#C20000'> >> ERROR: FH Prediction lib is missing! << </font>")
	return
end

if myHero.charName ~= "Yasuo" then return end

local CurVer = 1.1

function OnLoad()
	loadMinions()
	loadMenu()
	loadTS()
	loadVariables()
	
	PrintChat("<font color='#CCCCCC'> >> Yasuo Simple Q Stacker Loaded! << </font>")
end

function loadMinions()
	enemyMinion = minionManager(MINION_ENEMY, 600, player, MINION_SORT_HEALTH_DSC)
	jungleMinion = minionManager(MINION_JUNGLE, 600, player, MINION_SORT_HEALTH_DSC)
end

function loadMenu()
	YasQ = scriptConfig("Yasuo Simple Q Stacker", "Yas_Q_Stacker")
	YasQ:addParam("info", "          ------------------------------", SCRIPT_PARAM_INFO, "")
	YasQ:addParam("stackQ", "Auto Stack Q", SCRIPT_PARAM_ONKEYTOGGLE, false, string.byte("S"))
	YasQ:addParam("eChamp", "Stack Q on Enemy", SCRIPT_PARAM_ONOFF, true)
	YasQ:addParam("eMin", "Stack Q on Minions", SCRIPT_PARAM_ONOFF, true)
	YasQ:addParam("jMin", "Stack Q on Jungle Mobs", SCRIPT_PARAM_ONOFF, true)
	YasQ:addParam("eTurr", "Stack Q under Enemy Turrets", SCRIPT_PARAM_ONOFF, false)
	YasQ:addParam("info1", "          ------------------------------", SCRIPT_PARAM_INFO, "")
	YasQ:addParam("HPcheck", "DO NOT Stack Q if my HP < %", SCRIPT_PARAM_ONOFF, false)
	YasQ:addParam("myMinHP", "", SCRIPT_PARAM_SLICE, 20, 1, 100, 0)
	YasQ:addParam("info2", "          ------------------------------", SCRIPT_PARAM_INFO, "")	
	YasQ:addParam("info3", " >> Created by TheOneRU", SCRIPT_PARAM_INFO, "")
	YasQ:addParam("info4", " >> Version "..CurVer, SCRIPT_PARAM_INFO, "")

	YasQ:permaShow("stackQ")
end

function loadTS()
	ts = TargetSelector(TARGET_LESS_CAST_PRIORITY, 525, DAMAGE_PHYSICAL)
	ts.name = "Yasuo"
	YasQ:addTS(ts)
end

function loadVariables()
	
	AmIRecalling = 0
	Q3Ready = 0
	myHPperc = 0
	
end

function OnTick()
	if not myHero.dead then
	
		QREADY = (myHero:CanUseSpell(_Q) == READY)
		
		if not YasQ.HPcheck then
			mainFunc()			
		else
			HPcheck()
			if myHPperc >= YasQ.myMinHP then
				mainFunc()
			end
		end

	end
end

function OnUpdateBuff(unit, buff)
	if unit == nil or buff == nil then 
		return
	end
	if buff.name == "recall" and unit.isMe then
		AmIRecalling = 1
	end
	if buff.name == "YasuoQ3W" and unit.isMe then
		Q3Ready = 1
	end
end

function OnRemoveBuff(unit, buff)
	if unit == nil or buff == nil then 
		return
	end
	if buff.name == "recall" and unit.isMe then
		AmIRecalling = 0
	end
	if buff.name == "YasuoQ3W" and unit.isMe then
		Q3Ready = 0
	end
end

function HPcheck()
	if YasQ.HPcheck then
		myHPperc = (myHero.health / myHero.maxHealth * 100)
	end
end

function mainFunc()
	if not YasQ.eTurr and not UnderTurret(myHero) then
		if YasQ.stackQ and QREADY and Q3Ready == 0 and AmIRecalling == 0 then
			ts:update()
			if YasQ.eChamp and ts.target then
				useQ()
			end
			if YasQ.eChamp and not ts.target then
				if YasQ.eMin then
					enemyMinion:update()
					useQm()
				end
				if YasQ.jMin then
					jungleMinion:update()				
					useQjm()
				end
			end
			if not YasQ.eChamp then
				if YasQ.eMin then
					enemyMinion:update()
					useQm()
				end
				if YasQ.jMin then
					jungleMinion:update()				
					useQjm()
				end
			end
		else
			return
		end
	end
	
	if YasQ.eTurr then
		if YasQ.stackQ and QREADY and Q3Ready == 0 and AmIRecalling == 0 then
			ts:update()
			if YasQ.eChamp and ts.target then
				useQ()
			end
			if YasQ.eChamp and not ts.target then
				if YasQ.eMin then
					enemyMinion:update()
					useQm()
				end
				if YasQ.jMin then
					jungleMinion:update()				
					useQjm()
				end
			end
			if not YasQ.eChamp then
				if YasQ.eMin then
					enemyMinion:update()
					useQm()
				end
				if YasQ.jMin then
					jungleMinion:update()				
					useQjm()
				end
			end
		else
			return
		end
	end
end

function useQ()
	local collision = FHPrediction.IntersectsWindwall(myHero, ts.target)
	if not collision then
		if FHPrediction.HasPreset("Q") then
			local pos, hc, info = FHPrediction.GetPrediction("Q", ts.target)
			if hc > 0 then
				CastSpell(_Q, pos.x, pos.z)
			end
		end
	end
end

function useQm()
	for j, minion in pairs(enemyMinion.objects) do
		if ValidTarget(minion) and minion.visible and not minion.dead then
			local collision = FHPrediction.IntersectsWindwall(myHero, minion)
			if not collision then
				if FHPrediction.PredictHealth(minion, 0.25) >= 1 then
					if FHPrediction.HasPreset("Q") then
					local pos, hc, info = FHPrediction.GetPrediction("Q", minion)
						if hc > 0 then
							CastSpell(_Q, pos.x, pos.z)
						end
					end
				else
					return
				end
			end
		end
	end
end

function useQjm()
	for j, jungleMinion in pairs(jungleMinion.objects) do
		if ValidTarget(jungleMinion) and jungleMinion.visible and not jungleMinion.dead then
			local collision = FHPrediction.IntersectsWindwall(myHero, jungleMinion)
			if not collision then
				if FHPrediction.HasPreset("Q") then
				local pos, hc, info = FHPrediction.GetPrediction("Q", jungleMinion)
					if hc > 0 then
						CastSpell(_Q, pos.x, pos.z)
					end
				end
			end
		end
	end
end