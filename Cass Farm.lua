if myHero.charName ~= "Cassiopeia" then return end

function OnLoad()
	JACmain = scriptConfig("Cass Farm", "Cass_farm")
	JACmain:addParam("autoFarm", "Auto Farm with E", SCRIPT_PARAM_ONKEYTOGGLE, false, string.byte("T"))

	JACmain:permaShow("autoFarm")

	enemyMinion = minionManager(MINION_ENEMY, 1000, player, MINION_SORT_HEALTH_DSC)
end

function OnTick()
	if not myHero.dead then

		EREADY = (myHero:CanUseSpell(_E) == READY)

		if JACmain.autoFarm then
			autoFarm()
		end
	end
end

function autoFarm()
	enemyMinion:update()
	if next(enemyMinion.objects)~= nil then
		for j, minion in pairs(enemyMinion.objects) do
			if minion.valid then
				local edamage = getDmg("E", minion, myHero, 3)
				if edamage>=minion.health then
					CastE(minion)
				end
			end
		end
	end
end

function CastE(target)
	if ValidTarget(target) then
		if GetDistance(target) <= 700 and EREADY then
			if TargetHaveBuff("cassiopeianoxiousblastpoison", target) or TargetHaveBuff("cassiopeiamiasmapoison", target) or TargetHaveBuff("toxicshotparticle", target) or TargetHaveBuff("bantamtraptarget", target) or TargetHaveBuff("poisontrailtarget", target) or TargetHaveBuff("deadlyvenom", target) then
				CastSpell(_E, target)
			end
		end
	else
		return
	end
end